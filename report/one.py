import json
import boto3
import os
import openpyxl
import pymysql
import logging
import sys
import rds_config
import datetime
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
import dateutil.tz

rds_host  = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name
port = 3306

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

data_source_bucket = 'tokyustay-customize-report'
data_source_key = "sample_n.xlsx"

print("Connection to RDS MySQL start ")

# parameter_date = "2021-07"

# import requests
def lambda_handler(event, context):

    master_id = "10565"
    
    # try:
    #     conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    # except pymysql.MySQLError as e:
    #     logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    #     logger.error(e)
    #     sys.exit()

    logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")

    print("lambda_handler start aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ")

    print("parameter check start  22222222222222222222")
    print(event)
    print("parameter check end 33333333333333333333333")

    data = event["queryStringParameters"]

    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

        ###parameter 
    
    start_date = data['start_date']
    end_date = data['end_date']
    hotel_id = data['hotel_id']
    month = data['month']
    period_check = data['period_check']
    private_info = data['private_info']

    if period_check == "true":
        first_date = start_date+" 00:00:00"
        last_date = end_date+" 23:59:59"
        disply_month = start_date.split('-')[1]
        year = start_date.split('-')[0]

        if int(disply_month) == 1:
            last_year = str(int(year)-1)
            last_month_first_date = last_year + "-12-01 00:00:00"
            last_month_last_date = last_year + "-12-31 23:59:59"
        else:
            last_month_first_date = year + "-" + str(int(disply_month)-1) + "-01 00:00:00"
            last_month_last_date = year + "-" + str(int(disply_month)-1) + "-31 23:59:59"        
    else:
        first_date = month+"-01 00:00:00"
        last_date = month+"-31 23:59:59"
        disply_month = month.split('-')[1]
        year = month.split('-')[0]

        if int(disply_month) == 1:
            last_year = str(int(year)-1)
            last_month_first_date = last_year + "-12-01 00:00:00"
            last_month_last_date = last_year + "-12-31 23:59:59"
        else:
            last_month_first_date = year + "-" + str(int(disply_month)-1) + "-01 00:00:00"
            last_month_last_date = year + "-" + str(int(disply_month)-1) + "-31 23:59:59" 

    print("last month first date : " +  last_month_first_date)
    print("last month last date : " + last_month_last_date)

    keyword_mst_seq = data['hotel_id']
    stay_keyword_mst_seq = data['hotel_id']
    
    site_names = get_site_name()
    forder_mst_1 = get_folders_mst_1()
    forder_mst_2 = get_folders_mst_2()
    forder_mst_3 = get_folders_mst_3()
    forder_name = get_folders_status(master_id)
    forder_seqs = list(forder_name.keys())
    forder_seqs_str = ','.join(forder_seqs)
    chanel_list = get_created_chanel_list(master_id)

#     情報カード
    comment_list = get_mng_comment_list(master_id, stay_keyword_mst_seq, first_date, last_date, forder_seqs_str,'','')

#     WEBアンケート
    survey_comment_list = get_mng_comment_list(master_id, stay_keyword_mst_seq, first_date, last_date, forder_seqs_str,'survey','')

#     OTAクチコミ
    kuchikomi_list = get_mng_comment_list(master_id, stay_keyword_mst_seq, first_date, last_date, forder_seqs_str,'ota','')

#     先月
    last_month_comment_list = get_mng_comment_list(master_id, stay_keyword_mst_seq, last_month_first_date, last_month_last_date, forder_seqs_str,'','OK')

    source_bucket_obj = s3.Bucket(data_source_bucket)
    source_bucket_obj.download_file('temp/' + data_source_key, '/tmp/' + data_source_key)
    
    logger.info("excel write start")
    wb = openpyxl.load_workbook('/tmp/' + data_source_key)
    
    ws1 = wb.get_sheet_by_name('情報カード')
    ws1['A1'] = disply_month + "月　情報カードコメント集約" 

    ws1_row_num = 7
    for row in comment_list:

        comment_status_1 = get_folders_mst_name(master_id,str(row[14]),forder_mst_1)
        comment_status_2 = get_folders_mst_name(master_id,str(row[14]),forder_mst_2)
        comment_status_3 = get_folders_mst_name(master_id,str(row[14]),forder_mst_3)
        
        ws1_row_num_str = str(ws1_row_num)
        comment = ""
        comment_tmp = row[1]
        comment_tmp = comment_tmp.split('--PSINC--REPCHECKER--SEPARATOR--')
        comment = comment_tmp[0]
        
        # print("989898989898989898989898")
        # print(comment_tmp[0])
        # print("989898989898989898989898")
        
        ws1['A'+ws1_row_num_str] = row[0].strftime('%Y-%m-%d')
        
        if private_info == "1":
            if row[15] is None:
                ws1['B'+ws1_row_num_str] = ""
            else:
                ws1['B'+ws1_row_num_str] = row[15]
        else:
            ws1['B'+ws1_row_num_str] = ""

        ws1['C'+ws1_row_num_str] = comment
        
        if row[4] is None:
            ws1['D'+ws1_row_num_str] = ""
            ws1['E'+ws1_row_num_str] = ""
            ws1['F'+ws1_row_num_str] = ""
            ws1['G'+ws1_row_num_str] = ""
            ws1['H'+ws1_row_num_str] = comment_status_1
            ws1['I'+ws1_row_num_str] = comment_status_2
            ws1['J'+ws1_row_num_str] = comment_status_3
        else:
            ws1['D'+ws1_row_num_str] = row[4].strftime('%Y-%m-%d')
            if private_info == "1":
                ws1['E'+ws1_row_num_str] = row[5]
            else:
                ws1['E'+ws1_row_num_str] = ""
            ws1['F'+ws1_row_num_str] = row[7]

            try:
                ws1['G'+ws1_row_num_str] = forder_name[str(row[3])]
            except KeyError as e:
                ws1['G'+ws1_row_num_str] = ""
            else:
                ws1['G'+ws1_row_num_str] = forder_name[str(row[3])]
            
            ws1['H'+ws1_row_num_str] = comment_status_1
            ws1['I'+ws1_row_num_str] = comment_status_2
            ws1['J'+ws1_row_num_str] = comment_status_3
        
        ws1_row_num+=1

    ws2 = wb.get_sheet_by_name('WEBアンケート')
    ws2['A1'] = disply_month + "月　webアンケートコメント集約"

    ws2_row_num = 7
    for row in survey_comment_list:

        survey_status_1 = get_folders_mst_name(master_id,str(row[14]),forder_mst_1)
        survey_status_2 = get_folders_mst_name(master_id,str(row[14]),forder_mst_2)
        survey_status_3 = get_folders_mst_name(master_id,str(row[14]),forder_mst_3)
        
        ws2_row_num_str = str(ws2_row_num)
        comment = ""
        comment_tmp = row[1]
        comment_tmp = comment_tmp.split('--PSINC--REPCHECKER--SEPARATOR--')
        comment = comment_tmp[0]
        
        ws2['A'+ws2_row_num_str] = row[0].strftime('%Y-%m-%d')
        
        # if private_info == "1":
        #     if row[16] is None:
        #         ws2['B'+ws2_row_num_str] = ""
        #     else:
        #         ws2['B'+ws2_row_num_str] = row[16]
        # else:
        #     ws2['B'+ws2_row_num_str] = ""

        if row[16] is None:
                ws2['B'+ws2_row_num_str] = ""
        else:
            ws2['B'+ws2_row_num_str] = row[16]

        ws2['C'+ws2_row_num_str] = comment
        
        if row[4] is None:
            ws2['D'+ws2_row_num_str] = ""
            ws2['E'+ws2_row_num_str] = ""
            ws2['F'+ws2_row_num_str] = ""
            ws2['G'+ws2_row_num_str] = ""
            ws2['H'+ws2_row_num_str] = survey_status_1
            ws2['I'+ws2_row_num_str] = survey_status_2
            ws2['J'+ws2_row_num_str] = survey_status_3
        else:
            ws2['D'+ws2_row_num_str] = row[4].strftime('%Y-%m-%d')
            if private_info == "1":
                ws2['E'+ws2_row_num_str] = row[5]
            else:
                ws2['E'+ws2_row_num_str] = ""
            ws2['F'+ws2_row_num_str] = row[7]

            try:
                ws2['G'+ws2_row_num_str] = forder_name[str(row[3])]
            except KeyError as e:
                ws2['G'+ws2_row_num_str] = ""
            else:
                ws2['G'+ws2_row_num_str] = forder_name[str(row[3])]
            
            ws2['H'+ws2_row_num_str] = survey_status_1
            ws2['I'+ws2_row_num_str] = survey_status_2
            ws2['J'+ws2_row_num_str] = survey_status_3
        
        ws2_row_num+=1


    ws3 = wb.get_sheet_by_name('OTAクチコミ')
    ws3['C1'] = disply_month + "月 OTA口コミ集約"

    ws3_row_num = 7
    for row in kuchikomi_list:
        ws3_row_num_str = str(ws3_row_num)
        comment_tmp = row[1]
        comment_tmp = comment_tmp.split('--PSINC--REPCHECKER--SEPARATOR--')
        comment = comment_tmp[0]
        
        kuchikomi_status_1 = get_folders_mst_name(master_id,str(row[14]),forder_mst_1)
        kuchikomi_status_2 = get_folders_mst_name(master_id,str(row[14]),forder_mst_2)
        kuchikomi_status_3 = get_folders_mst_name(master_id,str(row[14]),forder_mst_3)
        
        ws3['A'+ws3_row_num_str] = row[0].strftime('%Y-%m-%d')
        ws3['B'+ws3_row_num_str] = str(row[9])
        ws3['C'+ws3_row_num_str] = row[11]+comment
        
        if row[4] is None:
            ws3['D'+ws3_row_num_str] = ""
            ws3['E'+ws3_row_num_str] = ""
            ws3['F'+ws3_row_num_str] = ""
            ws3['G'+ws3_row_num_str] = ""
            ws3['H'+ws3_row_num_str] = kuchikomi_status_1
            ws3['I'+ws3_row_num_str] = kuchikomi_status_2
            ws3['J'+ws3_row_num_str] = kuchikomi_status_3
        else:
            ws3['D'+ws3_row_num_str] = row[4].strftime('%Y-%m-%d')
            if private_info == "1":
                ws3['E'+ws3_row_num_str] = row[5]
            else:
                ws3['E'+ws3_row_num_str] = ""
            ws3['F'+ws3_row_num_str] = row[7]
            
            try:
                ws3['G'+ws3_row_num_str] = forder_name[str(row[3])]
            except KeyError as e:
                ws3['G'+ws3_row_num_str] = ""
            else:
                ws3['G'+ws3_row_num_str] = forder_name[str(row[3])]

            ws3['H'+ws3_row_num_str] = kuchikomi_status_1
            ws3['I'+ws3_row_num_str] = kuchikomi_status_2
            ws3['J'+ws3_row_num_str] = kuchikomi_status_3
        
        ws3_row_num+=1

    ws4 = wb.get_sheet_by_name('先月')
    ws4['A1'] = "先月　情報カードコメント集約"

    ws4_row_num = 7

    for row in last_month_comment_list:

        comment_status_1 = get_folders_mst_name(master_id,str(row[14]),forder_mst_1)
        comment_status_2 = get_folders_mst_name(master_id,str(row[14]),forder_mst_2)
        comment_status_3 = get_folders_mst_name(master_id,str(row[14]),forder_mst_3)
        
        ws4_row_num_str = str(ws4_row_num)
        comment = ""
        comment_tmp = row[1]
        comment_tmp = comment_tmp.split('--PSINC--REPCHECKER--SEPARATOR--')
        comment = comment_tmp[0]
        
        ws4['A'+ws4_row_num_str] = row[0].strftime('%Y-%m-%d')
        
        if private_info == "1":
            if row[15] is None:
                ws4['B'+ws4_row_num_str] = ""
            else:
                ws4['B'+ws4_row_num_str] = row[15]
        else:
            ws4['B'+ws4_row_num_str] = ""

        ws4['C'+ws4_row_num_str] = comment
        
        if row[4] is None:
            ws4['D'+ws4_row_num_str] = ""
            ws4['E'+ws4_row_num_str] = ""
            ws4['F'+ws4_row_num_str] = ""
            ws4['G'+ws4_row_num_str] = ""
            ws4['H'+ws4_row_num_str] = comment_status_1
            ws4['I'+ws4_row_num_str] = comment_status_2
            ws4['J'+ws4_row_num_str] = comment_status_3
        else:
            ws4['D'+ws4_row_num_str] = row[4].strftime('%Y-%m-%d')
            if private_info == "1":
                ws4['E'+ws4_row_num_str] = row[5]
            else:
                ws4['E'+ws4_row_num_str] = ""
            ws4['F'+ws4_row_num_str] = row[7]

            try:
                ws4['G'+ws4_row_num_str] = forder_name[str(row[3])]
            except KeyError as e:
                ws4['G'+ws4_row_num_str] = ""
            else:
                ws4['G'+ws4_row_num_str] = forder_name[str(row[3])]
            
            ws4['H'+ws4_row_num_str] = comment_status_1
            ws4['I'+ws4_row_num_str] = comment_status_2
            ws4['J'+ws4_row_num_str] = comment_status_3
        
        ws4_row_num+=1
    
    hotel_id = data['hotel_id']
    month = data['month']
    private_info = data['private_info']
    upload_folder = "excel/"

    excel_file_name = "report01_" + hotel_id +  "_" + month + "_" + private_info
    
    wb.save('/tmp/' + excel_file_name + ".xlsx")
    print("excel write end")
    
    print("excel upload start")
    s3_client.upload_file('/tmp/' + excel_file_name + ".xlsx", data_source_bucket, upload_folder + excel_file_name + ".xlsx")
    print("excel upload end")

    print("lambda_handler end ")

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "report 01",
            # "location": ip.text.replace("\n", "")
        }),
    }

def get_site_name():
    arr = ['jalan','rakuten','ikyu']
    return arr

def get_folders_mst_1():
   
   forder_mst_names = {}
   forder_mst_names['4'] = '宿泊'
   forder_mst_names['5'] = '清掃'
   forder_mst_names['6'] = 'レストラン'
   forder_mst_names['7'] = '調理'
   forder_mst_names['8'] = '施設'
   forder_mst_names['9'] = 'その他'

   return forder_mst_names

def get_folders_mst_2():
   
   forder_mst_names = {}
   forder_mst_names['1'] = '気づき'
   forder_mst_names['2'] = 'お褒め'
   forder_mst_names['3'] = 'ご不満'

   return forder_mst_names

def get_folders_mst_3():
   
   forder_mst_names = {}
   forder_mst_names['10'] = '次年度予算措置'
   forder_mst_names['11'] = '修理修繕'
   forder_mst_names['12'] = '教育研修'
   forder_mst_names['13'] = '現地にてフォロー'
   forder_mst_names['14'] = '施設共通事項'

   return forder_mst_names


def test_folder():
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += "* FROM mng_folders_trn_rc order by seq desc limit 1 "    
    print("check")
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    return rows

def get_folders_mst_name(master_id,mng_kuchikomi_mst_seq,folders_mst):
    
    forder_mst_seqs = list(folders_mst.keys())
    forder_mst_seqs_str = ','.join(forder_mst_seqs)
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " mng_folders_mst_seq "
    sql += " FROM mng_folders_trn_rc "
    sql += " WHERE master_id = '"+master_id+"' "
    sql += " AND mng_kuchikomi_mst_seq = '"+mng_kuchikomi_mst_seq+"' "
    sql += " AND mng_folders_mst_seq IN ( "+forder_mst_seqs_str+" ) "
    
    cursor.execute(sql)
    folder_mst_seq = cursor.fetchone()
    cursor.close()
    conn.close()
    
    folder_mst_name = ""
    
    if folder_mst_seq is None:
        folder_mst_name = ""
    else:
        folder_mst_name_tmp = str(folder_mst_seq[0])
        if folder_mst_name_tmp is None:
            folder_mst_name = ""
        else:
            if folders_mst[folder_mst_name_tmp] is None:
                 folder_mst_name = ""
            else:
                folder_mst_name = folders_mst[folder_mst_name_tmp]
    
    return folder_mst_name


def get_folders_status(master_id):
   
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " seq AS folder_seq , folder_name "
    sql += " FROM mng_folders_status "
    sql += " WHERE master_id = '"+master_id+"' "
    sql += " AND visible_flg = 'on'  "
    
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()
    
    forder_names = {}
    for row in rows:
        if row[1] == '対応完了：A':
            forder_names[str(row[0])] = 'A'
        elif row[1] == '対応中：B':
            forder_names[str(row[0])] = 'B'
        elif row[1] == '対応検討中：C':
            forder_names[str(row[0])] = 'C'
        elif row[1] == '見送り案件：D':
            forder_names[str(row[0])] = 'D'
        elif row[1] == 'その他：E':
            forder_names[str(row[0])] = 'E'
        elif row[1] == '次年度案件：Ｆ':
            forder_names[str(row[0])] = 'F'
    
    return forder_names

def get_mng_delete():
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
     
    sql = " SELECT mng_kuchikomi_mst_seq "
    sql += " FROM mng_delete "
    sql += " WHERE master_id = 10565"
    
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    
    mng_kuchikomi_mst_seqs = {}
        
    for row in result:
        mng_kuchikomi_mst_seqs[str(row[0])] = row[0]
    
    mng_kuchikomi_mst_seqs_str = ','.join(mng_kuchikomi_mst_seqs)
    
    return mng_kuchikomi_mst_seqs_str

def get_created_chanel_list(master_id):
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " seq,name "
    sql += " FROM mng_manual_mst "
    sql += " WHERE master_id = '"+ master_id +"' "
    sql += " AND delete_flg = '0' "
    sql += " ORDER BY display_order  "
    
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    conn.close()

    chanel_list = {}
    for row in rows:
        chanel_list[str(row[0])] =row[1] 
    
    return chanel_list

def get_mng_comment_list(master_id, keyword_mst_seq, first_date, last_date,forder_seqs_str,check_ota,last_month_check):
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()

    mng_delete_ids_str = get_mng_delete()
    
    sql = "select  "
    sql += " M.post_date, M.comment AS kuchikomi, M.keyword_mst_seq, C.status,"
    sql += " C.regist,C.add_person, S.folder_name, C.comment,   C.comment_kind, M.site_id,M.post_date,M.add_info,M.site_id ,M.add_info ,M.seq , K.post_name , K.post_title "
    sql += " FROM mng_kuchikomi_mst AS M "
    sql +=  " LEFT JOIN mng_comment AS C ON C.mng_kuchikomi_mst_seq = M.seq   "
    sql +=  " LEFT JOIN customize_kuchikomi AS K ON K.mng_kuchikomi_mst_seq = M.seq   "
    sql +=  " LEFT JOIN mng_folders_status AS S ON C.status = S.seq  "
    sql += " WHERE M.keyword_mst_seq = '" + keyword_mst_seq + "' "
    
    if check_ota == "ota":
        sql += " AND site_id IN ('rakuten','jalan','ikyu','yahoo','booking')"
    elif check_ota == "survey":
        sql += " AND site_id REGEXP '^[0-9]*$' AND site_id = '275' "
    else:
        sql += " AND site_id REGEXP '^[0-9]*$' AND site_id = '272' "
    
    if last_month_check == "OK":
        sql += " AND ( S.folder_name =  '対応中：B' or S.folder_name =  '対応検討中：C' )"
    else:
        sql += " AND DATE(M.post_date) >= '" + first_date + "'"

    sql += " AND DATE(M.post_date) <= '"+last_date+"'"
    sql += " AND M.seq NOT IN (" + mng_delete_ids_str + ") "

    sql += " ORDER BY M.post_date,C.regist "

    print(sql)
    
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()

    return result

def get_info_card_cnt_list(keyword_mst_seq, first_date, last_date):
    
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
    cursor = conn.cursor()
    
    sql = " SELECT site_id, COUNT(*) AS cnt "
    sql += " FROM mng_kuchikomi_mst "
    sql += " WHERE DATE(post_date) >= '" + first_date + "' AND DATE(post_date) <= '"+last_date+"'"
    sql += " AND site_id REGEXP '^[0-9]*$'"
    sql += " AND keyword_mst_seq = '" + keyword_mst_seq + "' "
    sql += " AND delete_flg = 0"
    sql += " GROUP BY site_id "
    
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    
    return result

def get_current_date():
        return datetime.datetime.now(dateutil.tz.gettz('Asia/Tokyo'))


