import json
import boto3
import os
import openpyxl
import pymysql
import logging
import sys
import rds_config
import datetime
from datetime import date, timedelta
from dateutil.relativedelta import relativedelta
import dateutil.tz


# parameter_date = "2021-07"
# start_date_tmp = parameter_date+"-01"
# start_date = datetime.datetime.strptime(start_date_tmp,'%Y-%m-%d')+relativedelta(months=0)

rds_host  = rds_config.db_host
name = rds_config.db_username
password = rds_config.db_password
db_name = rds_config.db_name
port = 3306

logger = logging.getLogger()
logger.setLevel(logging.INFO)

s3 = boto3.resource('s3')
s3_client = boto3.client('s3')

data_source_bucket = 'tokyustay-customize-report'
data_source_key = "report02_sample.xlsx"

print("Connection to RDS MySQL start ")

try:
    conn = pymysql.connect(host=rds_host, user=name, passwd=password, db=db_name, connect_timeout=5,charset='utf8')
except pymysql.MySQLError as e:
    logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
    logger.error(e)
    sys.exit()

logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")

master_id = "10565"

# import requests
def lambda_handler(event, context):

    print("lambda_handler start ")

    print("parameter check start")
    print(event)
    print("parameter check end")
    
    # data = {}
    # if event is None:
    #     data['year'] = "2021"
    # else:
    #     data = event["queryStringParameters"]
    # data = "2021"

    # data = event["queryStringParameters"]

    # print("data check start")
    # print(data)
    # print("data check end")

    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    ###parameter 
    year = "2021"
    own_hotels = get_own_hotels()
    site_names = get_site_name()
    forder_mst_1 = get_folders_mst_1()

    forder_mst_1_seqs = list(forder_mst_1.keys())
    forder_mst_1_seqs_str = ','.join(forder_mst_1_seqs)
    print("line 106")
    source_bucket_obj = s3.Bucket(data_source_bucket)
    source_bucket_obj.download_file('temp/' + data_source_key, '/tmp/' + data_source_key)

    logger.info("excel write start")
    wb = openpyxl.load_workbook('/tmp/' + data_source_key)
    
    print(wb.sheetnames)
    
#     ws1 = wb.get_sheet_by_name('sample')
#     print("line 116")
# #     copy sheet 1
#     target = wb.copy_worksheet(ws1)
#     target.title = "総合"
#     print("line 120")
#     for hotel_name in own_hotels.values():
#         target = wb.copy_worksheet(ws1)
#         target.title = hotel_name
#     print("line 124")
#     wb.remove_sheet(wb.get_sheet_by_name('sample'))
#     print("line 126")
#     print(wb.sheetnames)

    #     共通データ
    default_cell = ['D','E','F','H','I','J','L','M','N','Q','P','R']
    default_cell_2 = ['X','Y','Z','AB','AC','AD','AF','AG','AH','AJ','AK','AL']
    
# 1. own_hotels str_seq
    
    own_hotels_seqs = list(own_hotels.keys())
    own_hotels_seqs_str = ','.join(own_hotels_seqs)
    print("line 137")
# 2. data create
    start_date_tmp = year+"-04-01"
    forder_seqs_str = ""
    forder_mst_temp = ""
    print("line 142")
    
    for hotel_id,hotel_name in own_hotels.items():
        ws2 = wb[hotel_name]
        print("hotel name : " + hotel_name + " ======= ")
        ws2['A1'] = hotel_name
        
    
    print("総合 データ  作成　START")
#     
#   総合
#     
    total_info_card_cnt = {}
    total_fb_cnt = {}
    total_info_1_cnt  = {}
    total_info_1_cnt_l1 = {}
    total_info_1_cnt_l2 = {}
    total_info_1_cnt_l3 = {}
    total_info_1_cnt_l4 = {}
    total_info_1_cnt_l5 = {}
    total_info_1_cnt_l6 = {}
    total_info_2_cnt  = {}
    total_info_2_cnt_l1 = {}
    total_info_2_cnt_l2 = {}
    total_info_2_cnt_l3 = {}
    total_info_2_cnt_l4 = {}
    total_info_2_cnt_l5 = {}
    total_info_2_cnt_l6 = {}
    total_info_3_cnt  = {}
    total_info_3_cnt_l1 = {}./
    total_info_3_cnt_l2 = {}
    total_info_3_cnt_l3 = {}
    total_info_3_cnt_l4 = {}
    total_info_3_cnt_l5 = {}
    total_info_3_cnt_l6 = {}
    
    total_info_1_cnt_r1 = {}
    total_info_1_cnt_r2 = {}
    total_info_1_cnt_r3 = {}
    total_info_1_cnt_r4 = {}
    total_info_1_cnt_r5 = {}
    
    total_jalan_cnt_list = {}
    total_rakuten_cnt_list = {}
    total_yahoo_cnt_list = {}
    total_ikyu_cnt_list = {}
    total_expedia_cnt_list = {}
    total_booking_cnt_list = {}
    total_tripadvisor_cnt_list = {}
    
    total_jalan_cnt_list_l1 = {}
    total_jalan_cnt_list_l2 = {}
    total_jalan_cnt_list_l3 = {}
    total_jalan_cnt_list_l4 = {}
    total_jalan_cnt_list_l5 = {}
    total_jalan_cnt_list_l6 = {}
    
    total_rakuten_cnt_list_l1 = {}
    total_rakuten_cnt_list_l2 = {}
    total_rakuten_cnt_list_l3 = {}
    total_rakuten_cnt_list_l4 = {}
    total_rakuten_cnt_list_l5 = {}
    total_rakuten_cnt_list_l6 = {}
    
    total_yahoo_cnt_list_l1 = {}
    total_yahoo_cnt_list_l2 = {}
    total_yahoo_cnt_list_l3 = {}
    total_yahoo_cnt_list_l4 = {}
    total_yahoo_cnt_list_l5 = {}
    total_yahoo_cnt_list_l6 = {}
    
    total_ikyu_cnt_list_l1 = {}
    total_ikyu_cnt_list_l2 = {}
    total_ikyu_cnt_list_l3 = {}
    total_ikyu_cnt_list_l4 = {}
    total_ikyu_cnt_list_l5 = {}
    total_ikyu_cnt_list_l6 = {}
    
    total_expedia_cnt_list_l1 = {}
    total_expedia_cnt_list_l2 = {}
    total_expedia_cnt_list_l3 = {}
    total_expedia_cnt_list_l4 = {}
    total_expedia_cnt_list_l5 = {}
    total_expedia_cnt_list_l6 = {}
    
    total_booking_cnt_list_l1 = {}
    total_booking_cnt_list_l2 = {}
    total_booking_cnt_list_l3 = {}
    total_booking_cnt_list_l4 = {}
    total_booking_cnt_list_l5 = {}
    total_booking_cnt_list_l6 = {}
    
    total_tripadvisor_cnt_list_l1 = {}
    total_tripadvisor_cnt_list_l2 = {}
    total_tripadvisor_cnt_list_l3 = {}
    total_tripadvisor_cnt_list_l4 = {}
    total_tripadvisor_cnt_list_l5 = {}
    total_tripadvisor_cnt_list_l6 = {}
    
    total_jalan_cnt_list_r1 = {}
    total_jalan_cnt_list_r2 = {}
    total_jalan_cnt_list_r3 = {}
    total_jalan_cnt_list_r4 = {}
    total_jalan_cnt_list_r5 = {}
    
    total_rakuten_cnt_list_r1 = {}
    total_rakuten_cnt_list_r2 = {}
    total_rakuten_cnt_list_r3 = {}
    total_rakuten_cnt_list_r4 = {}
    total_rakuten_cnt_list_r5 = {}
    
    total_yahoo_cnt_list_r1 = {}
    total_yahoo_cnt_list_r2 = {}
    total_yahoo_cnt_list_r3 = {}
    total_yahoo_cnt_list_r4 = {}
    total_yahoo_cnt_list_r5 = {}
    
    total_ikyu_cnt_list_r1 = {}
    total_ikyu_cnt_list_r2 = {}
    total_ikyu_cnt_list_r3 = {}
    total_ikyu_cnt_list_r4 = {}
    total_ikyu_cnt_list_r5 = {}
    
    total_expedia_cnt_list_r1 = {}
    total_expedia_cnt_list_r2 = {}
    total_expedia_cnt_list_r3 = {}
    total_expedia_cnt_list_r4 = {}
    total_expedia_cnt_list_r5 = {}
    
    total_booking_cnt_list_r1 = {}
    total_booking_cnt_list_r2 = {}
    total_booking_cnt_list_r3 = {}
    total_booking_cnt_list_r4 = {}
    total_booking_cnt_list_r5 = {}
    
    total_tripadvisor_cnt_list_r1 = {}
    total_tripadvisor_cnt_list_r2 = {}
    total_tripadvisor_cnt_list_r3 = {}
    total_tripadvisor_cnt_list_r4 = {}
    total_tripadvisor_cnt_list_r5 = {}
    print("line 284")
    for i in range(12):
        start_date_check = datetime.datetime.strptime(start_date_tmp,'%Y-%m-%d')+relativedelta(months=i)
        start_date_str = start_date_check.strftime('%Y-%m-%d')
        start_year_month_end_date = start_date_str
        start_year_month_end_date = datetime.datetime.strptime(start_year_month_end_date,'%Y-%m-%d')+relativedelta(months=1)
        start_year_month_end_date = start_year_month_end_date-relativedelta(days=1)
        
        first_date = str(start_date_check)
        last_date = str(start_year_month_end_date)
        
        print("first_date : " + first_date)
        print("last_date : " + last_date)
        
#         総合　-　総合
        total_info_card_cnt[i] = get_info_card_cnt(own_hotels_seqs_str, first_date, last_date)
        
#         FB件数
        total_fb_cnt[i] = get_fb_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_1_seqs_str,'','')
        
        total_info_1_cnt[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '1','',"")
        total_info_1_cnt_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4,1','',"")
        total_info_1_cnt_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5,1','',"")
        total_info_1_cnt_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6,1','',"")
        total_info_1_cnt_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7,1','',"")
        total_info_1_cnt_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8,1','',"")
        total_info_1_cnt_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9,1','',"")
        
        total_info_2_cnt[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '2','',"")
        total_info_2_cnt_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4,2','',"")
        total_info_2_cnt_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5,2','',"")
        total_info_2_cnt_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6,2','',"")
        total_info_2_cnt_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7,2','',"")
        total_info_2_cnt_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8,2','',"")
        total_info_2_cnt_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9,2','',"")
        
        total_info_3_cnt[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '3','',"")
        total_info_3_cnt_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4,3','',"")
        total_info_3_cnt_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5,3','',"")
        total_info_3_cnt_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6,3','',"")
        total_info_3_cnt_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7,3','',"")
        total_info_3_cnt_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8,3','',"")
        total_info_3_cnt_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9,3','',"")
        
        total_info_1_cnt_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','',"")
        total_info_1_cnt_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','',"")
        total_info_1_cnt_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','',"")
        total_info_1_cnt_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','',"")
        total_info_1_cnt_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','',"")
        
#         OTA
        total_jalan_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"jalan")
        total_rakuten_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"rakuten")
        total_yahoo_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"yahoo")
        total_ikyu_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"ikyu")
        total_expedia_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"expedia")
        total_booking_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"booking")
        total_tripadvisor_cnt_list[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, forder_mst_temp,'ota',"tripadvisor")
        
        total_jalan_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"jalan")
        total_jalan_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"jalan")
        total_jalan_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"jalan")
        total_jalan_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"jalan")
        total_jalan_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"jalan")
        total_jalan_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"jalan")
        
        total_rakuten_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"rakuten")
        total_rakuten_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"rakuten")
        total_rakuten_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"rakuten")
        total_rakuten_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"rakuten")
        total_rakuten_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"rakuten")
        total_rakuten_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"rakuten")
        
        total_yahoo_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"yahoo")
        total_yahoo_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"yahoo")
        total_yahoo_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"yahoo")
        total_yahoo_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"yahoo")
        total_yahoo_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"yahoo")
        total_yahoo_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"yahoo")
        
        total_ikyu_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"ikyu")
        total_ikyu_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"ikyu")
        total_ikyu_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"ikyu")
        total_ikyu_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"ikyu")
        total_ikyu_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"ikyu")
        total_ikyu_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"ikyu")
        
        total_expedia_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"expedia")
        total_expedia_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"expedia")
        total_expedia_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"expedia")
        total_expedia_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"expedia")
        total_expedia_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"expedia")
        total_expedia_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"expedia")
        
        total_booking_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"booking")
        total_booking_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"booking")
        total_booking_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"booking")
        total_booking_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"booking")
        total_booking_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"booking")
        total_booking_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"booking")
        
        total_tripadvisor_cnt_list_l1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '4','ota',"tripadvisor")
        total_tripadvisor_cnt_list_l2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '5','ota',"tripadvisor")
        total_tripadvisor_cnt_list_l3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '6','ota',"tripadvisor")
        total_tripadvisor_cnt_list_l4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '7','ota',"tripadvisor")
        total_tripadvisor_cnt_list_l5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '8','ota',"tripadvisor")
        total_tripadvisor_cnt_list_l6[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '9','ota',"tripadvisor")
        
        total_jalan_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"jalan")
        total_jalan_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"jalan")
        total_jalan_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"jalan")
        total_jalan_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"jalan")
        total_jalan_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"jalan")
        
        total_rakuten_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"rakuten")
        total_rakuten_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"rakuten")
        total_rakuten_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"rakuten")
        total_rakuten_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"rakuten")
        total_rakuten_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"rakuten")
        
        total_yahoo_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"yahoo")
        total_yahoo_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"yahoo")
        total_yahoo_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"yahoo")
        total_yahoo_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"yahoo")
        total_yahoo_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"yahoo")
        
        total_ikyu_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"ikyu")
        total_ikyu_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"ikyu")
        total_ikyu_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"ikyu")
        total_ikyu_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"ikyu")
        total_ikyu_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"ikyu")
        
        total_expedia_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"expedia")
        total_expedia_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"expedia")
        total_expedia_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"expedia")
        total_expedia_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"expedia")
        total_expedia_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"expedia")
        
        total_booking_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"booking")
        total_booking_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"booking")
        total_booking_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"booking")
        total_booking_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"booking")
        total_booking_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"booking")
        
        total_tripadvisor_cnt_list_r1[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '10','ota',"tripadvisor")
        total_tripadvisor_cnt_list_r2[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '11','ota',"tripadvisor")
        total_tripadvisor_cnt_list_r3[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '12','ota',"tripadvisor")
        total_tripadvisor_cnt_list_r4[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '13','ota',"tripadvisor")
        total_tripadvisor_cnt_list_r5[i] = get_mng_cnt(master_id, own_hotels_seqs_str, first_date, last_date, '14','ota',"tripadvisor")
    
    print("総合 データ  作成　END")
    
    print("総合 EXCEL  作成　 LEFT start")

    #     総合
    ws1 = wb.get_sheet_by_name('総合')
    
#     Excel 作成
    cell_num = 0
    
#     左表
    for cell_item in default_cell:
        
#         総合 - 合計
        ws1[cell_item+'5'] = total_info_card_cnt[cell_num]
        ws1[cell_item+'6'] = total_fb_cnt[cell_num]
        
        ws1[cell_item+'8'] = total_info_1_cnt[cell_num]
        ws1[cell_item+'23'] = total_info_1_cnt_l1[cell_num]
        ws1[cell_item+'38'] = total_info_1_cnt_l2[cell_num]
        ws1[cell_item+'53'] = total_info_1_cnt_l3[cell_num]
        ws1[cell_item+'68'] = total_info_1_cnt_l4[cell_num]
        ws1[cell_item+'83'] = total_info_1_cnt_l5[cell_num]
        ws1[cell_item+'98'] = total_info_1_cnt_l6[cell_num]
        
        ws1[cell_item+'9'] = total_info_2_cnt[cell_num]
        ws1[cell_item+'24'] = total_info_2_cnt_l1[cell_num]
        ws1[cell_item+'39'] = total_info_2_cnt_l2[cell_num]
        ws1[cell_item+'54'] = total_info_2_cnt_l3[cell_num]
        ws1[cell_item+'69'] = total_info_2_cnt_l4[cell_num]
        ws1[cell_item+'84'] = total_info_2_cnt_l5[cell_num]
        ws1[cell_item+'99'] = total_info_2_cnt_l6[cell_num]
        
        ws1[cell_item+'10'] = total_info_3_cnt[cell_num]
        ws1[cell_item+'25'] = total_info_3_cnt_l1[cell_num]
        ws1[cell_item+'40'] = total_info_3_cnt_l2[cell_num]
        ws1[cell_item+'55'] = total_info_3_cnt_l3[cell_num]
        ws1[cell_item+'70'] = total_info_3_cnt_l4[cell_num]
        ws1[cell_item+'85'] = total_info_3_cnt_l5[cell_num]
        ws1[cell_item+'100'] = total_info_3_cnt_l6[cell_num]
        
#         OTA
        ws1[cell_item+'12'] = total_jalan_cnt_list[cell_num]
        ws1[cell_item+'13'] = total_rakuten_cnt_list[cell_num]
        ws1[cell_item+'14'] = total_yahoo_cnt_list[cell_num]
        ws1[cell_item+'15'] = total_ikyu_cnt_list[cell_num]
        ws1[cell_item+'16'] = total_expedia_cnt_list[cell_num]
        ws1[cell_item+'17'] = total_booking_cnt_list[cell_num]
        ws1[cell_item+'18'] = total_tripadvisor_cnt_list[cell_num]
        
        ws1[cell_item+'27'] = total_jalan_cnt_list_l1[cell_num]
        ws1[cell_item+'28'] = total_rakuten_cnt_list_l1[cell_num]
        ws1[cell_item+'29'] = total_yahoo_cnt_list_l1[cell_num]
        ws1[cell_item+'30'] = total_ikyu_cnt_list_l1[cell_num]
        ws1[cell_item+'31'] = total_expedia_cnt_list_l1[cell_num]
        ws1[cell_item+'32'] = total_booking_cnt_list_l1[cell_num]
        ws1[cell_item+'33'] = total_tripadvisor_cnt_list_l1[cell_num]
        
        ws1[cell_item+'42'] = total_jalan_cnt_list_l2[cell_num]
        ws1[cell_item+'43'] = total_rakuten_cnt_list_l2[cell_num]
        ws1[cell_item+'44'] = total_yahoo_cnt_list_l2[cell_num]
        ws1[cell_item+'45'] = total_ikyu_cnt_list_l2[cell_num]
        ws1[cell_item+'46'] = total_expedia_cnt_list_l2[cell_num]
        ws1[cell_item+'47'] = total_booking_cnt_list_l2[cell_num]
        ws1[cell_item+'48'] = total_tripadvisor_cnt_list_l2[cell_num]
        
        ws1[cell_item+'57'] = total_jalan_cnt_list_l3[cell_num]
        ws1[cell_item+'58'] = total_rakuten_cnt_list_l3[cell_num]
        ws1[cell_item+'59'] = total_yahoo_cnt_list_l3[cell_num]
        ws1[cell_item+'60'] = total_ikyu_cnt_list_l3[cell_num]
        ws1[cell_item+'61'] = total_expedia_cnt_list_l3[cell_num]
        ws1[cell_item+'62'] = total_booking_cnt_list_l3[cell_num]
        ws1[cell_item+'63'] = total_tripadvisor_cnt_list_l3[cell_num]
        
        ws1[cell_item+'72'] = total_jalan_cnt_list_l4[cell_num]
        ws1[cell_item+'73'] = total_rakuten_cnt_list_l4[cell_num]
        ws1[cell_item+'74'] = total_yahoo_cnt_list_l4[cell_num]
        ws1[cell_item+'75'] = total_ikyu_cnt_list_l4[cell_num]
        ws1[cell_item+'76'] = total_expedia_cnt_list_l4[cell_num]
        ws1[cell_item+'77'] = total_booking_cnt_list_l4[cell_num]
        ws1[cell_item+'78'] = total_tripadvisor_cnt_list_l4[cell_num]
        
        ws1[cell_item+'87'] = total_jalan_cnt_list_l5[cell_num]
        ws1[cell_item+'88'] = total_rakuten_cnt_list_l5[cell_num]
        ws1[cell_item+'89'] = total_yahoo_cnt_list_l5[cell_num]
        ws1[cell_item+'90'] = total_ikyu_cnt_list_l5[cell_num]
        ws1[cell_item+'91'] = total_expedia_cnt_list_l5[cell_num]
        ws1[cell_item+'92'] = total_booking_cnt_list_l5[cell_num]
        ws1[cell_item+'93'] = total_tripadvisor_cnt_list_l5[cell_num]
        
        ws1[cell_item+'102'] = total_jalan_cnt_list_l6[cell_num]
        ws1[cell_item+'103'] = total_rakuten_cnt_list_l6[cell_num]
        ws1[cell_item+'104'] = total_yahoo_cnt_list_l6[cell_num]
        ws1[cell_item+'105'] = total_ikyu_cnt_list_l6[cell_num]
        ws1[cell_item+'106'] = total_expedia_cnt_list_l6[cell_num]
        ws1[cell_item+'107'] = total_booking_cnt_list_l6[cell_num]
        ws1[cell_item+'108'] = total_tripadvisor_cnt_list_l6[cell_num]
        
        cell_num += 1
        
    print("総合 EXCEL  作成　 LEFT END")
    print("line 535")
    print("総合 EXCEL  作成　 RIGHT start")
    
    cell_right_num = 0
#     右表
    for cell_item in default_cell_2:
        
        ws1[cell_item+'15'] = total_info_1_cnt_r1[cell_right_num]
        ws1[cell_item+'27'] = total_info_1_cnt_r2[cell_right_num]
        ws1[cell_item+'39'] = total_info_1_cnt_r3[cell_right_num]
        ws1[cell_item+'51'] = total_info_1_cnt_r4[cell_right_num]
        ws1[cell_item+'63'] = total_info_1_cnt_r5[cell_right_num]
        
        ws1[cell_item+'17'] = total_jalan_cnt_list_r1[cell_right_num]
        ws1[cell_item+'18'] = total_rakuten_cnt_list_r1[cell_right_num]
        ws1[cell_item+'19'] = total_yahoo_cnt_list_r1[cell_right_num]
        ws1[cell_item+'20'] = total_ikyu_cnt_list_r1[cell_right_num]
        ws1[cell_item+'21'] = total_expedia_cnt_list_r1[cell_right_num]
        ws1[cell_item+'22'] = total_booking_cnt_list_r1[cell_right_num]
        ws1[cell_item+'23'] = total_tripadvisor_cnt_list_r1[cell_right_num]
        
        ws1[cell_item+'29'] = total_jalan_cnt_list_r2[cell_right_num]
        ws1[cell_item+'30'] = total_rakuten_cnt_list_r2[cell_right_num]
        ws1[cell_item+'31'] = total_yahoo_cnt_list_r2[cell_right_num]
        ws1[cell_item+'32'] = total_ikyu_cnt_list_r2[cell_right_num]
        ws1[cell_item+'33'] = total_expedia_cnt_list_r2[cell_right_num]
        ws1[cell_item+'34'] = total_booking_cnt_list_r2[cell_right_num]
        ws1[cell_item+'35'] = total_tripadvisor_cnt_list_r2[cell_right_num]
        
        ws1[cell_item+'41'] = total_jalan_cnt_list_r3[cell_right_num]
        ws1[cell_item+'42'] = total_rakuten_cnt_list_r3[cell_right_num]
        ws1[cell_item+'43'] = total_yahoo_cnt_list_r3[cell_right_num]
        ws1[cell_item+'44'] = total_ikyu_cnt_list_r3[cell_right_num]
        ws1[cell_item+'45'] = total_expedia_cnt_list_r3[cell_right_num]
        ws1[cell_item+'46'] = total_booking_cnt_list_r3[cell_right_num]
        ws1[cell_item+'47'] = total_tripadvisor_cnt_list_r3[cell_right_num]
        
        ws1[cell_item+'53'] = total_jalan_cnt_list_r4[cell_right_num]
        ws1[cell_item+'54'] = total_rakuten_cnt_list_r4[cell_right_num]
        ws1[cell_item+'55'] = total_yahoo_cnt_list_r4[cell_right_num]
        ws1[cell_item+'56'] = total_ikyu_cnt_list_r4[cell_right_num]
        ws1[cell_item+'57'] = total_expedia_cnt_list_r4[cell_right_num]
        ws1[cell_item+'58'] = total_booking_cnt_list_r4[cell_right_num]
        ws1[cell_item+'59'] = total_tripadvisor_cnt_list_r4[cell_right_num]
        
        ws1[cell_item+'65'] = total_jalan_cnt_list_r5[cell_right_num]
        ws1[cell_item+'66'] = total_rakuten_cnt_list_r5[cell_right_num]
        ws1[cell_item+'67'] = total_yahoo_cnt_list_r5[cell_right_num]
        ws1[cell_item+'68'] = total_ikyu_cnt_list_r5[cell_right_num]
        ws1[cell_item+'69'] = total_expedia_cnt_list_r5[cell_right_num]
        ws1[cell_item+'70'] = total_booking_cnt_list_r5[cell_right_num]
        ws1[cell_item+'71'] = total_tripadvisor_cnt_list_r5[cell_right_num]
        
        cell_right_num += 1
        
    print("総合 EXCEL  作成　 RIGHT END")
    
    print("各ホテル DATA - EXCEL  作成　start")
#     各ホテル
    for hotel_id,hotel_name in own_hotels.items():
        
        ws3 = wb[hotel_name]
        
        print("hotel name : " + hotel_name + " ======= ")
        
        hotel_info_card_cnt = {}
        hotel_fb_cnt = {}
        hotel_info_1_cnt  = {}
        hotel_info_1_cnt_l1 = {}
        hotel_info_1_cnt_l2 = {}
        hotel_info_1_cnt_l3 = {}
        hotel_info_1_cnt_l4 = {}
        hotel_info_1_cnt_l5 = {}
        hotel_info_1_cnt_l6 = {}
        hotel_info_2_cnt  = {}
        hotel_info_2_cnt_l1 = {}
        hotel_info_2_cnt_l2 = {}
        hotel_info_2_cnt_l3 = {}
        hotel_info_2_cnt_l4 = {}
        hotel_info_2_cnt_l5 = {}
        hotel_info_2_cnt_l6 = {}
        hotel_info_3_cnt  = {}
        hotel_info_3_cnt_l1 = {}
        hotel_info_3_cnt_l2 = {}
        hotel_info_3_cnt_l3 = {}
        hotel_info_3_cnt_l4 = {}
        hotel_info_3_cnt_l5 = {}
        hotel_info_3_cnt_l6 = {}
        
        hotel_info_1_cnt_r1 = {}
        hotel_info_1_cnt_r2 = {}
        hotel_info_1_cnt_r3 = {}
        hotel_info_1_cnt_r4 = {}
        hotel_info_1_cnt_r5 = {}
        
        hotel_jalan_cnt_list = {}
        hotel_rakuten_cnt_list = {}
        hotel_yahoo_cnt_list = {}
        hotel_ikyu_cnt_list = {}
        hotel_expedia_cnt_list = {}
        hotel_booking_cnt_list = {}
        hotel_tripadvisor_cnt_list = {}
        
        hotel_jalan_cnt_list_l1 = {}
        hotel_jalan_cnt_list_l2 = {}
        hotel_jalan_cnt_list_l3 = {}
        hotel_jalan_cnt_list_l4 = {}
        hotel_jalan_cnt_list_l5 = {}
        hotel_jalan_cnt_list_l6 = {}
        
        hotel_rakuten_cnt_list_l1 = {}
        hotel_rakuten_cnt_list_l2 = {}
        hotel_rakuten_cnt_list_l3 = {}
        hotel_rakuten_cnt_list_l4 = {}
        hotel_rakuten_cnt_list_l5 = {}
        hotel_rakuten_cnt_list_l6 = {}
        
        hotel_yahoo_cnt_list_l1 = {}
        hotel_yahoo_cnt_list_l2 = {}
        hotel_yahoo_cnt_list_l3 = {}
        hotel_yahoo_cnt_list_l4 = {}
        hotel_yahoo_cnt_list_l5 = {}
        hotel_yahoo_cnt_list_l6 = {}
        
        hotel_ikyu_cnt_list_l1 = {}
        hotel_ikyu_cnt_list_l2 = {}
        hotel_ikyu_cnt_list_l3 = {}
        hotel_ikyu_cnt_list_l4 = {}
        hotel_ikyu_cnt_list_l5 = {}
        hotel_ikyu_cnt_list_l6 = {}
        
        hotel_expedia_cnt_list_l1 = {}
        hotel_expedia_cnt_list_l2 = {}
        hotel_expedia_cnt_list_l3 = {}
        hotel_expedia_cnt_list_l4 = {}
        hotel_expedia_cnt_list_l5 = {}
        hotel_expedia_cnt_list_l6 = {}
        
        hotel_booking_cnt_list_l1 = {}
        hotel_booking_cnt_list_l2 = {}
        hotel_booking_cnt_list_l3 = {}
        hotel_booking_cnt_list_l4 = {}
        hotel_booking_cnt_list_l5 = {}
        hotel_booking_cnt_list_l6 = {}
        
        hotel_tripadvisor_cnt_list_l1 = {}
        hotel_tripadvisor_cnt_list_l2 = {}
        hotel_tripadvisor_cnt_list_l3 = {}
        hotel_tripadvisor_cnt_list_l4 = {}
        hotel_tripadvisor_cnt_list_l5 = {}
        hotel_tripadvisor_cnt_list_l6 = {}
        
        hotel_jalan_cnt_list_r1 = {}
        hotel_jalan_cnt_list_r2 = {}
        hotel_jalan_cnt_list_r3 = {}
        hotel_jalan_cnt_list_r4 = {}
        hotel_jalan_cnt_list_r5 = {}
        
        hotel_rakuten_cnt_list_r1 = {}
        hotel_rakuten_cnt_list_r2 = {}
        hotel_rakuten_cnt_list_r3 = {}
        hotel_rakuten_cnt_list_r4 = {}
        hotel_rakuten_cnt_list_r5 = {}
        
        hotel_yahoo_cnt_list_r1 = {}
        hotel_yahoo_cnt_list_r2 = {}
        hotel_yahoo_cnt_list_r3 = {}
        hotel_yahoo_cnt_list_r4 = {}
        hotel_yahoo_cnt_list_r5 = {}
        
        hotel_ikyu_cnt_list_r1 = {}
        hotel_ikyu_cnt_list_r2 = {}
        hotel_ikyu_cnt_list_r3 = {}
        hotel_ikyu_cnt_list_r4 = {}
        hotel_ikyu_cnt_list_r5 = {}
        
        hotel_expedia_cnt_list_r1 = {}
        hotel_expedia_cnt_list_r2 = {}
        hotel_expedia_cnt_list_r3 = {}
        hotel_expedia_cnt_list_r4 = {}
        hotel_expedia_cnt_list_r5 = {}
        
        hotel_booking_cnt_list_r1 = {}
        hotel_booking_cnt_list_r2 = {}
        hotel_booking_cnt_list_r3 = {}
        hotel_booking_cnt_list_r4 = {}
        hotel_booking_cnt_list_r5 = {}
        
        hotel_tripadvisor_cnt_list_r1 = {}
        hotel_tripadvisor_cnt_list_r2 = {}
        hotel_tripadvisor_cnt_list_r3 = {}
        hotel_tripadvisor_cnt_list_r4 = {}
        hotel_tripadvisor_cnt_list_r5 = {}
        
        for i in range(12):
            start_date_check = datetime.datetime.strptime(start_date_tmp,'%Y-%m-%d')+relativedelta(months=i)
            start_date_str = start_date_check.strftime('%Y-%m-%d')
            start_year_month_end_date = start_date_str
            start_year_month_end_date = datetime.datetime.strptime(start_year_month_end_date,'%Y-%m-%d')+relativedelta(months=1)
            start_year_month_end_date = start_year_month_end_date-relativedelta(days=1)
            
            first_date = str(start_date_check)
            last_date = str(start_year_month_end_date)
            
            #         総合　-　総合
            hotel_info_card_cnt[i] = get_info_card_cnt(hotel_id, first_date, last_date)
            
            #         FB件数
            hotel_fb_cnt[i] = get_fb_cnt(master_id, hotel_id, first_date, last_date, forder_mst_1_seqs_str,'','')
            
            hotel_info_1_cnt[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '1','',"")
            hotel_info_1_cnt_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4,1','',"")
            hotel_info_1_cnt_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5,1','',"")
            hotel_info_1_cnt_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6,1','',"")
            hotel_info_1_cnt_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7,1','',"")
            hotel_info_1_cnt_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8,1','',"")
            hotel_info_1_cnt_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9,1','',"")
            
            hotel_info_2_cnt[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '2','',"")
            hotel_info_2_cnt_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4,2','',"")
            hotel_info_2_cnt_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5,2','',"")
            hotel_info_2_cnt_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6,2','',"")
            hotel_info_2_cnt_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7,2','',"")
            hotel_info_2_cnt_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8,2','',"")
            hotel_info_2_cnt_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9,2','',"")
            
            hotel_info_3_cnt[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '3','',"")
            hotel_info_3_cnt_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4,3','',"")
            hotel_info_3_cnt_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5,3','',"")
            hotel_info_3_cnt_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6,3','',"")
            hotel_info_3_cnt_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7,3','',"")
            hotel_info_3_cnt_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8,3','',"")
            hotel_info_3_cnt_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9,3','',"")
            
            hotel_info_1_cnt_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','',"")
            hotel_info_1_cnt_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','',"")
            hotel_info_1_cnt_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','',"")
            hotel_info_1_cnt_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','',"")
            hotel_info_1_cnt_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','',"")
            
            
#         OTA
            hotel_jalan_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"jalan")
            hotel_rakuten_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"rakuten")
            hotel_yahoo_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"yahoo")
            hotel_ikyu_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"ikyu")
            hotel_expedia_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"expedia")
            hotel_booking_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"booking")
            hotel_tripadvisor_cnt_list[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, forder_mst_temp,'ota',"tripadvisor")
            
            hotel_jalan_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"jalan")
            hotel_jalan_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"jalan")
            hotel_jalan_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"jalan")
            hotel_jalan_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"jalan")
            hotel_jalan_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"jalan")
            hotel_jalan_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"jalan")
            
            hotel_rakuten_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"rakuten")
            hotel_rakuten_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"rakuten")
            hotel_rakuten_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"rakuten")
            hotel_rakuten_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"rakuten")
            hotel_rakuten_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"rakuten")
            hotel_rakuten_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"rakuten")
            
            hotel_yahoo_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"yahoo")
            hotel_yahoo_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"yahoo")
            hotel_yahoo_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"yahoo")
            hotel_yahoo_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"yahoo")
            hotel_yahoo_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"yahoo")
            hotel_yahoo_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"yahoo")
            
            hotel_ikyu_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"ikyu")
            hotel_ikyu_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"ikyu")
            hotel_ikyu_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"ikyu")
            hotel_ikyu_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"ikyu")
            hotel_ikyu_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"ikyu")
            hotel_ikyu_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"ikyu")
            
            hotel_expedia_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"expedia")
            hotel_expedia_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"expedia")
            hotel_expedia_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"expedia")
            hotel_expedia_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"expedia")
            hotel_expedia_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"expedia")
            hotel_expedia_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"expedia")
            
            hotel_booking_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"booking")
            hotel_booking_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"booking")
            hotel_booking_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"booking")
            hotel_booking_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"booking")
            hotel_booking_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"booking")
            hotel_booking_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"booking")
            
            hotel_tripadvisor_cnt_list_l1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '4','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_l2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '5','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_l3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '6','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_l4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '7','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_l5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '8','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_l6[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '9','ota',"tripadvisor")
            
            hotel_jalan_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"jalan")
            hotel_jalan_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"jalan")
            hotel_jalan_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"jalan")
            hotel_jalan_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"jalan")
            hotel_jalan_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"jalan")
            
            hotel_rakuten_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"rakuten")
            hotel_rakuten_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"rakuten")
            hotel_rakuten_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"rakuten")
            hotel_rakuten_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"rakuten")
            hotel_rakuten_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"rakuten")
            
            hotel_yahoo_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"yahoo")
            hotel_yahoo_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"yahoo")
            hotel_yahoo_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"yahoo")
            hotel_yahoo_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"yahoo")
            hotel_yahoo_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"yahoo")
            
            hotel_ikyu_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"ikyu")
            hotel_ikyu_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"ikyu")
            hotel_ikyu_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"ikyu")
            hotel_ikyu_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"ikyu")
            hotel_ikyu_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"ikyu")
            
            hotel_expedia_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"expedia")
            hotel_expedia_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"expedia")
            hotel_expedia_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"expedia")
            hotel_expedia_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"expedia")
            hotel_expedia_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"expedia")
            
            hotel_booking_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"booking")
            hotel_booking_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"booking")
            hotel_booking_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"booking")
            hotel_booking_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"booking")
            hotel_booking_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"booking")
            
            hotel_tripadvisor_cnt_list_r1[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '10','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_r2[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '11','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_r3[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '12','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_r4[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '13','ota',"tripadvisor")
            hotel_tripadvisor_cnt_list_r5[i] = get_mng_cnt(master_id, hotel_id, first_date, last_date, '14','ota',"tripadvisor")
            
            print("first_date : " + first_date)
            print("last_date : " + last_date)
            
        
        print("+++++++++++++++++++++++")
        
        
#     Excel 作成
        cell_num = 0
#     左表
        for cell_item in default_cell:
        
#         総合 - 合計
            ws3[cell_item+'5'] = hotel_info_card_cnt[cell_num]
            ws3[cell_item+'6'] = hotel_fb_cnt[cell_num]
            
            ws3[cell_item+'8'] = hotel_info_1_cnt[cell_num]
            ws3[cell_item+'23'] = hotel_info_1_cnt_l1[cell_num]
            ws3[cell_item+'38'] = hotel_info_1_cnt_l2[cell_num]
            ws3[cell_item+'53'] = hotel_info_1_cnt_l3[cell_num]
            ws3[cell_item+'68'] = hotel_info_1_cnt_l4[cell_num]
            ws3[cell_item+'83'] = hotel_info_1_cnt_l5[cell_num]
            ws3[cell_item+'98'] = hotel_info_1_cnt_l6[cell_num]
            
            ws3[cell_item+'9'] = hotel_info_2_cnt[cell_num]
            ws3[cell_item+'24'] = hotel_info_2_cnt_l1[cell_num]
            ws3[cell_item+'39'] = hotel_info_2_cnt_l2[cell_num]
            ws3[cell_item+'54'] = hotel_info_2_cnt_l3[cell_num]
            ws3[cell_item+'69'] = hotel_info_2_cnt_l4[cell_num]
            ws3[cell_item+'84'] = hotel_info_2_cnt_l5[cell_num]
            ws3[cell_item+'99'] = hotel_info_2_cnt_l6[cell_num]
            
            ws3[cell_item+'10'] = hotel_info_3_cnt[cell_num]
            ws3[cell_item+'25'] = hotel_info_3_cnt_l1[cell_num]
            ws3[cell_item+'40'] = hotel_info_3_cnt_l2[cell_num]
            ws3[cell_item+'55'] = hotel_info_3_cnt_l3[cell_num]
            ws3[cell_item+'70'] = hotel_info_3_cnt_l4[cell_num]
            ws3[cell_item+'85'] = hotel_info_3_cnt_l5[cell_num]
            ws3[cell_item+'100'] = hotel_info_3_cnt_l6[cell_num]
            
    #         OTA
            ws3[cell_item+'12'] = hotel_jalan_cnt_list[cell_num]
            ws3[cell_item+'13'] = hotel_rakuten_cnt_list[cell_num]
            ws3[cell_item+'14'] = hotel_yahoo_cnt_list[cell_num]
            ws3[cell_item+'15'] = hotel_ikyu_cnt_list[cell_num]
            ws3[cell_item+'16'] = hotel_expedia_cnt_list[cell_num]
            ws3[cell_item+'17'] = hotel_booking_cnt_list[cell_num]
            ws3[cell_item+'18'] = hotel_tripadvisor_cnt_list[cell_num]
            
            ws3[cell_item+'27'] = hotel_jalan_cnt_list_l1[cell_num]
            ws3[cell_item+'28'] = hotel_rakuten_cnt_list_l1[cell_num]
            ws3[cell_item+'29'] = hotel_yahoo_cnt_list_l1[cell_num]
            ws3[cell_item+'30'] = hotel_ikyu_cnt_list_l1[cell_num]
            ws3[cell_item+'31'] = hotel_expedia_cnt_list_l1[cell_num]
            ws3[cell_item+'32'] = hotel_booking_cnt_list_l1[cell_num]
            ws3[cell_item+'33'] = hotel_tripadvisor_cnt_list_l1[cell_num]
            
            ws3[cell_item+'42'] = hotel_jalan_cnt_list_l2[cell_num]
            ws3[cell_item+'43'] = hotel_rakuten_cnt_list_l2[cell_num]
            ws3[cell_item+'44'] = hotel_yahoo_cnt_list_l2[cell_num]
            ws3[cell_item+'45'] = hotel_ikyu_cnt_list_l2[cell_num]
            ws3[cell_item+'46'] = hotel_expedia_cnt_list_l2[cell_num]
            ws3[cell_item+'47'] = hotel_booking_cnt_list_l2[cell_num]
            ws3[cell_item+'48'] = hotel_tripadvisor_cnt_list_l2[cell_num]
            
            ws3[cell_item+'57'] = hotel_jalan_cnt_list_l3[cell_num]
            ws3[cell_item+'58'] = hotel_rakuten_cnt_list_l3[cell_num]
            ws3[cell_item+'59'] = hotel_yahoo_cnt_list_l3[cell_num]
            ws3[cell_item+'60'] = hotel_ikyu_cnt_list_l3[cell_num]
            ws3[cell_item+'61'] = hotel_expedia_cnt_list_l3[cell_num]
            ws3[cell_item+'62'] = hotel_booking_cnt_list_l3[cell_num]
            ws3[cell_item+'63'] = hotel_tripadvisor_cnt_list_l3[cell_num]
            
            ws3[cell_item+'72'] = hotel_jalan_cnt_list_l4[cell_num]
            ws3[cell_item+'73'] = hotel_rakuten_cnt_list_l4[cell_num]
            ws3[cell_item+'74'] = hotel_yahoo_cnt_list_l4[cell_num]
            ws3[cell_item+'75'] = hotel_ikyu_cnt_list_l4[cell_num]
            ws3[cell_item+'76'] = hotel_expedia_cnt_list_l4[cell_num]
            ws3[cell_item+'77'] = hotel_booking_cnt_list_l4[cell_num]
            ws3[cell_item+'78'] = hotel_tripadvisor_cnt_list_l4[cell_num]
            
            ws3[cell_item+'87'] = hotel_jalan_cnt_list_l5[cell_num]
            ws3[cell_item+'88'] = hotel_rakuten_cnt_list_l5[cell_num]
            ws3[cell_item+'89'] = hotel_yahoo_cnt_list_l5[cell_num]
            ws3[cell_item+'90'] = hotel_ikyu_cnt_list_l5[cell_num]
            ws3[cell_item+'91'] = hotel_expedia_cnt_list_l5[cell_num]
            ws3[cell_item+'92'] = hotel_booking_cnt_list_l5[cell_num]
            ws3[cell_item+'93'] = hotel_tripadvisor_cnt_list_l5[cell_num]
            
            ws3[cell_item+'102'] = hotel_jalan_cnt_list_l6[cell_num]
            ws3[cell_item+'103'] = hotel_rakuten_cnt_list_l6[cell_num]
            ws3[cell_item+'104'] = hotel_yahoo_cnt_list_l6[cell_num]
            ws3[cell_item+'105'] = hotel_ikyu_cnt_list_l6[cell_num]
            ws3[cell_item+'106'] = hotel_expedia_cnt_list_l6[cell_num]
            ws3[cell_item+'107'] = hotel_booking_cnt_list_l6[cell_num]
            ws3[cell_item+'108'] = hotel_tripadvisor_cnt_list_l6[cell_num]
            
            cell_num += 1
        
        cell_right_num = 0
#     右表
        for cell_item in default_cell_2:
            
            ws3[cell_item+'15'] = hotel_info_1_cnt_r1[cell_right_num]
            ws3[cell_item+'27'] = hotel_info_1_cnt_r2[cell_right_num]
            ws3[cell_item+'39'] = hotel_info_1_cnt_r3[cell_right_num]
            ws3[cell_item+'51'] = hotel_info_1_cnt_r4[cell_right_num]
            ws3[cell_item+'63'] = hotel_info_1_cnt_r5[cell_right_num]
            
            ws3[cell_item+'17'] = hotel_jalan_cnt_list_r1[cell_right_num]
            ws3[cell_item+'18'] = hotel_rakuten_cnt_list_r1[cell_right_num]
            ws3[cell_item+'19'] = hotel_yahoo_cnt_list_r1[cell_right_num]
            ws3[cell_item+'20'] = hotel_ikyu_cnt_list_r1[cell_right_num]
            ws3[cell_item+'21'] = hotel_expedia_cnt_list_r1[cell_right_num]
            ws3[cell_item+'22'] = hotel_booking_cnt_list_r1[cell_right_num]
            ws3[cell_item+'23'] = hotel_tripadvisor_cnt_list_r1[cell_right_num]
            
            ws3[cell_item+'29'] = hotel_jalan_cnt_list_r2[cell_right_num]
            ws3[cell_item+'30'] = hotel_rakuten_cnt_list_r2[cell_right_num]
            ws3[cell_item+'31'] = hotel_yahoo_cnt_list_r2[cell_right_num]
            ws3[cell_item+'32'] = hotel_ikyu_cnt_list_r2[cell_right_num]
            ws3[cell_item+'33'] = hotel_expedia_cnt_list_r2[cell_right_num]
            ws3[cell_item+'34'] = hotel_booking_cnt_list_r2[cell_right_num]
            ws3[cell_item+'35'] = hotel_tripadvisor_cnt_list_r2[cell_right_num]
            
            ws3[cell_item+'41'] = hotel_jalan_cnt_list_r3[cell_right_num]
            ws3[cell_item+'42'] = hotel_rakuten_cnt_list_r3[cell_right_num]
            ws3[cell_item+'43'] = hotel_yahoo_cnt_list_r3[cell_right_num]
            ws3[cell_item+'44'] = hotel_ikyu_cnt_list_r3[cell_right_num]
            ws3[cell_item+'45'] = hotel_expedia_cnt_list_r3[cell_right_num]
            ws3[cell_item+'46'] = hotel_booking_cnt_list_r3[cell_right_num]
            ws3[cell_item+'47'] = hotel_tripadvisor_cnt_list_r3[cell_right_num]
            
            ws3[cell_item+'53'] = hotel_jalan_cnt_list_r4[cell_right_num]
            ws3[cell_item+'54'] = hotel_rakuten_cnt_list_r4[cell_right_num]
            ws3[cell_item+'55'] = hotel_yahoo_cnt_list_r4[cell_right_num]
            ws3[cell_item+'56'] = hotel_ikyu_cnt_list_r4[cell_right_num]
            ws3[cell_item+'57'] = hotel_expedia_cnt_list_r4[cell_right_num]
            ws3[cell_item+'58'] = hotel_booking_cnt_list_r4[cell_right_num]
            ws3[cell_item+'59'] = hotel_tripadvisor_cnt_list_r4[cell_right_num]
            
            ws3[cell_item+'65'] = hotel_jalan_cnt_list_r5[cell_right_num]
            ws3[cell_item+'66'] = hotel_rakuten_cnt_list_r5[cell_right_num]
            ws3[cell_item+'67'] = hotel_yahoo_cnt_list_r5[cell_right_num]
            ws3[cell_item+'68'] = hotel_ikyu_cnt_list_r5[cell_right_num]
            ws3[cell_item+'69'] = hotel_expedia_cnt_list_r5[cell_right_num]
            ws3[cell_item+'70'] = hotel_booking_cnt_list_r5[cell_right_num]
            ws3[cell_item+'71'] = hotel_tripadvisor_cnt_list_r5[cell_right_num]
            
            cell_right_num += 1
    
    print("各ホテル DATA - EXCEL  作成　END")
    
    upload_folder = "excel/"

    excel_file_name = "report02_" + year
    
    wb.save('/tmp/' + excel_file_name + ".xlsx")
    print("excel write end")
    
    print("excel upload start")
    s3_client.upload_file('/tmp/' + excel_file_name + ".xlsx", data_source_bucket, upload_folder + excel_file_name + ".xlsx")
    print("excel upload end")

    print("lambda_handler end ")

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "report 02",
            # "location": ip.text.replace("\n", "")
        }),
    }


def get_site_name():
    arr = ['jalan','rakuten','yahoo','ikyu','expedia','booking','tripadvisor']
    return arr


def get_own_hotels():
    
    arr = {}
    arr['39'] = "ホテルニセコアルペン"
    arr['686'] = "裏磐梯 グランデコ 東急ホテル"
    arr['2080'] = "ホテルタングラム 斑尾東急リゾート"
    arr['6868'] = "蓼科東急ホテル"
    arr['16167'] = "ホテルハーヴェスト スキージャム勝山"
    arr['20788'] = "勝山ニューホテル"
    arr['61809'] = "旧軽井沢KIKYOキュリオ・コレクションbyヒルトン"
    arr['64038'] = "nol kyoto sanjo"
    
    return arr


def get_folders_mst_1():
   
   forder_mst_names = {}
   forder_mst_names['1'] = '気づき'
   forder_mst_names['2'] = 'お褒め'
   forder_mst_names['3'] = 'ご不満'

   return forder_mst_names


def get_folders_mst_2():
   
   forder_mst_names = {}
   forder_mst_names['4'] = '宿泊'
   forder_mst_names['5'] = '清掃'
   forder_mst_names['6'] = 'レストラン'
   forder_mst_names['7'] = '調理'
   forder_mst_names['8'] = '施設'
   forder_mst_names['9'] = 'その他'

   return forder_mst_names


def get_folders_mst_3():
   
   forder_mst_names = {}
   forder_mst_names['10'] = '次年度予算措置'
   forder_mst_names['11'] = '修理修繕'
   forder_mst_names['12'] = '教育研修'
   forder_mst_names['13'] = '現地にてフォロー'
   forder_mst_names['14'] = '施設共通事項'

   return forder_mst_names


def get_folders_mst_name(master_id,mng_kuchikomi_mst_seq,folders_mst):
    
    forder_mst_seqs = list(folders_mst.keys())
    forder_mst_seqs_str = ','.join(forder_mst_seqs)
    
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " mng_folders_mst_seq "
    sql += " FROM mng_folders_trn_rc "
    sql += " WHERE master_id = '"+master_id+"' "
    sql += " AND mng_kuchikomi_mst_seq = '"+mng_kuchikomi_mst_seq+"' "
    sql += " AND mng_folders_mst_seq IN ( "+forder_mst_seqs_str+" ) "
    
    cursor.execute(sql)
    folder_mst_seq = cursor.fetchone()
    cursor.close()
    
    folder_mst_name = ""
    
    if folder_mst_seq is None:
        folder_mst_name = ""
    else:
        folder_mst_name_tmp = str(folder_mst_seq[0])
        if folder_mst_name_tmp is None:
            folder_mst_name = ""
        else:
            if folders_mst[folder_mst_name_tmp] is None:
                 folder_mst_name = ""
            else:
                folder_mst_name = folders_mst[folder_mst_name_tmp]
    
    return folder_mst_name


def get_folders_status(master_id):
   
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " seq AS folder_seq , folder_name "
    sql += " FROM mng_folders_status "
    sql += " WHERE master_id = '"+master_id+"' "
    sql += " AND visible_flg = 'on'  "
    
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    
    forder_names = {}
    for row in rows:
        if row[1] == '対応完了：A':
            forder_names[str(row[0])] = 'A'
        elif row[1] == '対応中：B':
            forder_names[str(row[0])] = 'B'
        elif row[1] == '対応検討中：C':
            forder_names[str(row[0])] = 'C'
        elif row[1] == '見送り案件：D':
            forder_names[str(row[0])] = 'D'
        elif row[1] == 'その他：E':
            forder_names[str(row[0])] = 'E'
        elif row[1] == '次年度案件：Ｆ':
            forder_names[str(row[0])] = 'F'
    
    return forder_names

def get_created_chanel_list(master_id):
    
    cursor = conn.cursor()
    
    sql = " SELECT "
    sql += " seq,name "
    sql += " FROM mng_manual_mst "
    sql += " WHERE master_id = '"+ master_id +"' "
    sql += " AND delete_flg = '0' "
    sql += " ORDER BY display_order  "
    
    cursor.execute(sql)
    rows = cursor.fetchall()
    cursor.close()
    
    chanel_list = {}
    for row in rows:
        chanel_list[str(row[0])] =row[1] 
    
    return chanel_list

def get_mng_comment_list(master_id, keyword_mst_seq, first_date, last_date,forder_seqs_str,check_ota):
    
    cursor = conn.cursor()
    
    sql = "select  "
    sql += " M.post_date, M.comment AS kuchikomi, M.keyword_mst_seq, C.status,"
    sql += " C.regist,C.add_person, S.folder_name, C.comment,   C.comment_kind, M.site_id,M.post_date,M.add_info,M.site_id ,M.add_info ,M.seq , K.post_name "
#     sql += " FROM mng_comment AS C "
    sql += " FROM mng_kuchikomi_mst AS M "
    sql +=  " LEFT JOIN mng_comment AS C ON C.mng_kuchikomi_mst_seq = M.seq   "
    sql +=  " LEFT JOIN customize_kuchikomi AS K ON K.mng_kuchikomi_mst_seq = M.seq   "
    sql +=  " LEFT JOIN mng_folders_status AS S ON C.status = S.seq  "
#     sql += " WHERE S.visible_flg = 'on' "
#     sql += " AND C.master_id = '"+master_id+"' ";
    sql += " WHERE M.keyword_mst_seq = '" + keyword_mst_seq + "' "
    
#     sql = "select  "
#     sql += " M.post_date, M.comment AS kuchikomi, M.keyword_mst_seq, C.status,"
#     sql += " C.regist,C.add_person, S.folder_name, C.comment,   C.comment_kind, M.site_id,M.post_date,M.add_info,M.site_id ,M.add_info ,M.seq "
#     sql += " FROM mng_comment AS C "
#     sql +=  " LEFT JOIN mng_folders_status AS S ON C.status = S.seq  "
#     sql +=  " LEFT JOIN mng_kuchikomi_mst AS M ON C.mng_kuchikomi_mst_seq = M.seq   "
#     sql += " WHERE S.visible_flg = 'on' "
#     sql += " AND C.master_id = '"+master_id+"' ";
#     sql += " AND M.keyword_mst_seq = '" + keyword_mst_seq + "' "
    
#     if forder_seqs_str != "":
#         sql += " AND C.status IN (" + forder_seqs_str + ")"
    
    if check_ota == "ota":
        sql += " AND site_id IN ('rakuten','jalan','ikyu','yahoo','booking')"
    else:
        sql += " AND site_id REGEXP '^[0-9]*$'"
    
    sql += " AND DATE(M.post_date) >= '" + first_date + "' AND DATE(M.post_date) <= '"+last_date+"'"
    
    
    sql += " ORDER BY M.post_date,C.regist "
    

    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    
#     print(len(result))
#     sys.exit(0)
    
    return result


def get_fb_cnt(master_id, keyword_mst_seqs, first_date, last_date,forder_mst_seqs_str,check_ota,ota):
    
    cursor = conn.cursor()
    
    sql = "select  "
    sql += " COUNT(*) AS cnt "
    sql += " FROM mng_comment AS C "
    sql +=  " LEFT JOIN mng_kuchikomi_mst AS M ON C.mng_kuchikomi_mst_seq = M.seq   "
    sql += " WHERE M.keyword_mst_seq IN (" + keyword_mst_seqs + ") "
    sql += " AND C.master_id = '" + master_id + "'"
    
    if check_ota == "ota":
        sql += " AND M.site_id = '" + ota + "'"
    else:
        sql += " AND M.site_id REGEXP '^[0-9]*$'";
    
    sql += " AND DATE(M.post_date) >= '" + first_date + "' AND DATE(M.post_date) <= '"+last_date+"'"
    
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    
    if result is None:
        cnt = 0
    else:
        cnt = result[0]
    
    return cnt


def get_mng_cnt(master_id, keyword_mst_seqs, first_date, last_date,forder_mst_seqs_str,check_ota,ota):
    
    cursor = conn.cursor()
    
    sql = "select  "
    sql += " COUNT(*) AS cnt "
    sql += " FROM mng_kuchikomi_mst AS M "
#     sql +=  " LEFT JOIN mng_comment AS C ON C.mng_kuchikomi_mst_seq = M.seq   "
    sql +=  " LEFT JOIN mng_folders_trn_rc AS R ON R.mng_kuchikomi_mst_seq = M.seq   "
#     sql +=  " LEFT JOIN mng_folders_status AS S ON C.status = S.seq  "
    sql += " WHERE M.keyword_mst_seq IN (" + keyword_mst_seqs + ") "
    
    if forder_mst_seqs_str != "":
        sql += " AND R.master_id = '" + master_id + "'"
        sql += " AND R.mng_folders_mst_seq IN ( " + forder_mst_seqs_str + " ) "
    
    if check_ota == "ota":
        sql += " AND M.site_id = '" + ota + "'"
    else:
        sql += " AND M.site_id REGEXP '^[0-9]*$'";
    
    sql += " AND DATE(M.post_date) >= '" + first_date + "' AND DATE(M.post_date) <= '"+last_date+"'"
    
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    
    if result is None:
        cnt = 0
    else:
        cnt = result[0]
    
    return cnt


def get_info_card_cnt(keyword_mst_seqs, first_date, last_date):
    
    cursor = conn.cursor()
    
    sql = " SELECT COUNT(*) AS cnt "
    sql += " FROM mng_kuchikomi_mst "
    sql += " WHERE DATE(post_date) >= '" + first_date + "' AND DATE(post_date) <= '"+last_date+"'"
    sql += " AND site_id REGEXP '^[0-9]*$'"
    sql += " AND keyword_mst_seq IN (" + keyword_mst_seqs + ") "
    sql += " AND delete_flg = 0"
    sql += " AND site_id = 272"
    
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    
    if result is None:
        cnt = 0
    else:
        cnt = result[0]
    
    return cnt


def get_current_date():
        return datetime.datetime.now(dateutil.tz.gettz('Asia/Tokyo'))


